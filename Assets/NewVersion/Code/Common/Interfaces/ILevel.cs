﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ILevel
{
    void Build(Level level);
    void Initialize(GameSettings settings);
}
