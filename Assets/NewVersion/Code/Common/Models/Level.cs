﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class Level
{
    public int ID = 0;
    public List<LevelObject.LevelObjectData> Objects = new List<LevelObject.LevelObjectData>();

}
