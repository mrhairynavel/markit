﻿using System;
using UnityEngine;

public class LevelObject: MonoBehaviour
{
    #region NestedTypes
    [Serializable]
    public class LevelObjectData
    {
        public int     ID;
        public Vector3 Position; 
        public Vector3 Rotation;
    }
    #endregion

    private LevelObjectData _currentData;

    public void Initialize(int id)
    {
        _currentData = new LevelObjectData
        {
            ID = id
        };
    }

    public void SetRotation(Vector3 rotation)
    {
        transform.eulerAngles = rotation;
        _currentData.Rotation = transform.eulerAngles;
    }

    public void SetPosition(Vector3 position)
    {
        transform.position = position;
        _currentData.Position = transform.position;
    }

    public void Save()
    {
        var json = JsonUtility.ToJson(_currentData);
    }
}