﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "LevelObjectsSettings", menuName = "Game/Level Objects Settings")]
public class LevelObjectsSettings : ScriptableObject
{
    public List<LevelObject> Prefabs = new List<LevelObject>();


    public LevelObject GetLevelObject(int id)
    {
        return Prefabs[id];
    }
}
