﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "DirectorySettings", menuName = "Game/Directory Settings")]
public class DirectorySettings : ScriptableObject
{
    public string LevelsDirectory;
}
