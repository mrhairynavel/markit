﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "GameSettings", menuName = "Game/Game Settings")]
public class GameSettings : ScriptableObject
{
    public CharacterPresets CharacterSettings;
    public LevelObjectsSettings LevelObjectsSettings;
    public DirectorySettings DirectorySettings;
}