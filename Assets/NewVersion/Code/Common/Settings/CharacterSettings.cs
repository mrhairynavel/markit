﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName ="CharacterSettings", menuName = "Game/Character Settings")]
public class CharacterSettings : ScriptableObject
{
    [SerializeField]
    private float _speedLimit;
    public float SpeedLimit => _speedLimit;

    [SerializeField]
    private float _acceleration;
    public float Acceleration => _acceleration;

    [SerializeField]
    private float _friction;
    public float Friction => _friction;
}
