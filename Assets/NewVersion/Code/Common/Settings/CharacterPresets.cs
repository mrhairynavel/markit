﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CharacterPresets", menuName = "Game/Character Presets")]
public class CharacterPresets : ScriptableObject
{
    [SerializeField]
    private List<CharacterSettings> _settingsPresets = new List<CharacterSettings>();

    [SerializeField]
    private List<GameObject> _visualPresets = new List<GameObject>();


    public CharacterSettings GetSettings(int id)
    {
        return _settingsPresets[id];
    }

    public GameObject GetVisual(int id)
    {
        return _visualPresets[id];
    }
}
