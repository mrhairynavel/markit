﻿using Fiber;
using Fiber.Instances;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLoader : MonoBehaviour
{
    [SerializeField]
    private GameSettings _gameSettings;
    private GameData _gameData;

    private void Awake()
    {
        Initialize();
        FiberCore.Instances.FindPreloadedInstance();
    }

    private void OnInstanceChanged(Instance instance)
    {
        switch (instance.ID)
        {
            case 0:
                FiberCore.UI.GetScreen<UIScreen_Menu>().Show();
                FiberCore.UI.GetScreen<UIScreen_Game>().Hide();
                break;
            case 1:
                FiberCore.UI.GetScreen<UIScreen_Menu>().Hide();
                FiberCore.UI.GetScreen<UIScreen_Game>().Show();

                instance.As<Instance_Game>().Initialize(_gameSettings);

                break;
        }
    }

    private void Subscribe()
    {
        FiberCore.Instances.OnInstanceChanged += OnInstanceChanged;
    }

    private void Initialize()
    {
        Subscribe();
        LoadData();
    }

    private void LoadData()
    {
        FiberCore.PrefData.RegisterType<GameData>();
        FiberCore.PrefData.Load();
        FiberCore.PrefData.GetData(out _gameData);
    }


}
