﻿using Fiber;
using Fiber.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScreen_Menu : UIScreen
{
    [SerializeField]
    private Button _playButton;

    private void Awake()
    {
        BindButtons();
    }

    private void BindButtons()
    {
        _playButton.onClick.AddListener(OnPlayButton);
    }


    private void OnPlayButton()
    {
        FiberCore.Instances.LoadInstance(1);
    }
}
