﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CharacterCameraController : MonoBehaviour
{
    [SerializeField]
    private CharacterController _character;
    [SerializeField]
    private Camera _camera;

    public float Speed;
    public float ZoomLimit;
    public float ZoomStep;
    private float _currentZoom;
    private float fow;

    private void Start()
    {
        fow = _camera.fieldOfView;

       
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, _character.transform.position, Speed * Time.fixedDeltaTime);

        _currentZoom = Mathf.Clamp(ZoomStep * _character.CurrentSpeed / 5, 0, ZoomLimit);
        _camera.fieldOfView = fow + _currentZoom;
    }
}
