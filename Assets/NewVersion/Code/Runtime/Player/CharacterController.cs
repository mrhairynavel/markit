﻿using Fiber;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Fiber.Message;

public class CharacterController : MonoBehaviour
{
    [HideInInspector]
    public float CurrentSpeed;

    private Transform _skin;
    private CharacterSettings _settings;
    private Rigidbody _rigidbody;
    private bool _contacted;

   
    public void Initialize(GameSettings gameSettings)
    {
        Bind();
        TryGetComponent(out _rigidbody);

        var data = FiberCore.PrefData.GetData<GameData>();
        var characterPresets = gameSettings.CharacterSettings;

        _settings = characterPresets.GetSettings(data.ChatacterTypeID);
        _skin = Instantiate(characterPresets.GetVisual(data.CharacterVisualID), transform).transform;

    }



    private void Bind()
    {
        FiberCore.Message
            .Receive<Tracker_JoystickAxis>()
            .Subscribe(x => Move(x.AxisX, x.AxisY))
            .BindTo(this);

        FiberCore.Message
            .Receive<Tracker_PlayerCollisionContact>()
            .Subscribe(x => _contacted = x.CollisionObject != null)
            .BindTo(this);

        FiberCore.Message
            .Receive<Tracker_PlayerTriggerContact>()
            .Where(x => x.CollisionObject.CompareTag("Product"))
            .Subscribe(x => OnContactProduct(x.CollisionObject))
            .BindTo(this);
    }



    private void OnContactProduct(GameObject productObject)
    {
        Destroy(productObject);
        FiberCore.Instances.GetActiveInstance<Instance_Game>().AddScore();
    }



    private void Move(float xAxis, float yAxis)
    {
        var direction = Vector3.left * yAxis + Vector3.forward * xAxis;


        if (direction.magnitude > 0)
        {
            if (CurrentSpeed < _settings.SpeedLimit && !_contacted)
            {
                CurrentSpeed += _settings.Acceleration * Time.fixedDeltaTime;
            }

            _rigidbody.rotation = Quaternion.Lerp(_rigidbody.rotation, Quaternion.LookRotation(direction), _settings.Friction * Time.fixedDeltaTime);
            _skin.rotation = Quaternion.Lerp(_skin.rotation, Quaternion.LookRotation(direction), 10 * (direction.magnitude / 10) * Time.fixedDeltaTime);
        }
        else
        {

            if (!_contacted)
            {
                if (CurrentSpeed > 0.1f)
                {
                    CurrentSpeed -= _settings.Acceleration * Time.fixedDeltaTime;
                }
                else
                {
                    _rigidbody.velocity = Vector3.zero;
                }
            }
            else
            {
                if (CurrentSpeed > _settings.SpeedLimit / 3)
                    CurrentSpeed -= _settings.Acceleration * Time.fixedDeltaTime;
                else
                    CurrentSpeed = _settings.SpeedLimit / 3;
            }
        }

        _rigidbody.MovePosition(transform.position + transform.forward * CurrentSpeed * Time.fixedDeltaTime);
    }

}
