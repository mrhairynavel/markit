﻿using Fiber;
using Fiber.Instances;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instance_Game : Instance, ILevel
{
    private int _currentScore;
    private Level _currentLevel;
    private GameSettings _gameSettings;


    protected override void OnReady()
    {
       
    }

    protected override void OnUnload()
    {
        
    }

    public void Build(Level level)
    {
        _currentLevel = level;

        foreach (var item in _currentLevel.Objects)
        {
            var prefab = _gameSettings.LevelObjectsSettings.GetLevelObject(item.ID);
            var obj = Instantiate(prefab);
            obj.SetPosition(item.Position);
            obj.SetRotation(item.Rotation);
        }
    }

    void Start()
    {
        StartLevel();
    }

    public void StartLevel()
    {
        var player = FindObjectOfType<CharacterController>();
        if (player != null)
        {
            player.Initialize(_gameSettings);
        }
    }

    public void Initialize(GameSettings settings)
    {
        _gameSettings = settings;
    }

    public void AddScore()
    {
        _currentScore++;
        FiberCore.UI.GetScreen<UIScreen_Game>().ScoreLabel.text = _currentScore.ToString();
    }
}
