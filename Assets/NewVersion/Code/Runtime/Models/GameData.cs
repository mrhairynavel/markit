﻿using Fiber.PrefData;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData: BasePrefData
{
    public int ChatacterTypeID = 0;
    public int CharacterVisualID = 0;
}
