﻿using Fiber;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(VariableJoystick))]
public class JoystickTracker : MonoBehaviour
{
    private VariableJoystick _joystick;

    private float _currentX;
    private float _currentY;

    private void Awake()
    {
        TryGetComponent(out _joystick);
    }

    private void FixedUpdate()
    {
        //if(_joystick.Vertical != _currentY)
        {
            //_currentY = _joystick.Vertical;
            FiberCore.Message.Publish(new Tracker_JoystickAxis(_joystick.Horizontal, _joystick.Vertical));
        }

        //if (_joystick.Horizontal != _currentX)
        {
           // _currentX = _joystick.Horizontal;
           // FiberCore.Message.Publish(new Tracker_JoystickAxis(_currentX, _currentY));
        }
    }
}

public class Tracker_JoystickAxis
{
    public readonly float AxisX;
    public readonly float AxisY;

    public Tracker_JoystickAxis(float axisX, float axisY)
    {
        AxisX = axisX;
        AxisY = axisY;
    }
}
