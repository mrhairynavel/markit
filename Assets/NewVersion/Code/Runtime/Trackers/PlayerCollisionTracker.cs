﻿using Fiber;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody), typeof(Collider))]
public class PlayerCollisionTracker : MonoBehaviour
{
    List<GameObject> _collidedObjects = new List<GameObject>();
    List<GameObject> _triggeredObjects = new List<GameObject>();


    private void OnCollisionEnter(Collision collision)
    {
        if(!_collidedObjects.Contains(collision.gameObject))
        {
            _collidedObjects.Add(collision.gameObject);
            FiberCore.Message.Publish(new Tracker_PlayerCollisionContact(collision.gameObject));
        }
    }


    private void OnCollisionExit(Collision collision)
    {
        if (_collidedObjects.Contains(collision.gameObject))
        {
            _collidedObjects.Remove(collision.gameObject);

            if (_collidedObjects.Count == 0)
            {
                FiberCore.Message.Publish(new Tracker_PlayerCollisionContact(null));
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (!_triggeredObjects.Contains(other.gameObject))
        {
            _triggeredObjects.Add(other.gameObject);
            FiberCore.Message.Publish(new Tracker_PlayerTriggerContact(other.gameObject));
        }
    }


    private void OnTriggerExit(Collider other)
    {
        if (_triggeredObjects.Contains(other.gameObject))
        {
            _triggeredObjects.Remove(other.gameObject);

            if (_triggeredObjects.Count == 0)
            {
                FiberCore.Message.Publish(new Tracker_PlayerTriggerContact(null));
            }
        }
    }

}

public class Tracker_PlayerCollisionContact
{
    public GameObject CollisionObject;

    public Tracker_PlayerCollisionContact(GameObject collosionType)
    {
        CollisionObject = collosionType;
    }
}

public class Tracker_PlayerTriggerContact
{
    public GameObject CollisionObject;

    public Tracker_PlayerTriggerContact(GameObject collosionType)
    {
        CollisionObject = collosionType;
    }
}

