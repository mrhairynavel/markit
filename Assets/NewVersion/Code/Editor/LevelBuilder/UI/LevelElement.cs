﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class LevelElement : MonoBehaviour
{
    [SerializeField]
    private TMP_Text _label;
    [SerializeField]
    private Button _edit;

    public void Initialize(Level level, LevelBuilder builder)
    {
        _label.text = level.ID.ToString();
        _edit.onClick.AddListener(() => builder.Load(level));
    }
}
