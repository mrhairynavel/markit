﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEditor;

public class BuildElement : MonoBehaviour
{
    private Button _button;
    private GameObject _targetGO;

    public Image Preview;
    public TMP_Text Title;
    public TMP_Text ID;

   

    // Start is called before the first frame update
    void Initialize(int id)
    {
        

        TryGetComponent(out _button);

        Texture2D texture = AssetPreview.GetAssetPreview(_targetGO);
        Preview.overrideSprite = Sprite.Create(texture, new Rect(0,0, texture.width, texture.height), new Vector2(0,0));
        Title.text = _targetGO.name;
    }
}
