﻿using Bolt;
using Fiber.Instances;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class LevelBuilder : MonoBehaviour
{
    public GameSettings GameSettings;

    [SerializeField]
    private LevelElement _levelElement;
    [SerializeField]
    private Transform _levelElementPanel;
    [SerializeField]
    private BuildElement _buildElement;
    [SerializeField]
    private Transform _buildElementPanel;
    [SerializeField]
    private Button _createButton;

    [SerializeField]
    private Transform _listPanel;


    private ILevel _level;
    private List<string> _levels = new List<string>();


    private void Awake()
    {
        LoadLevels();
        _createButton.onClick.AddListener(Create);
    }


    public void LoadLevels()
    {
        var dir = Application.dataPath + @"/" + GameSettings.DirectorySettings.LevelsDirectory;
        var levelPaths = Directory.GetFiles(dir, "*.json", SearchOption.TopDirectoryOnly).ToList();

        foreach (var item in levelPaths)
        {
            using (StreamReader r = new StreamReader(item))
            {
                string json = r.ReadToEnd();
                _levels.Add(json);
            }
        }

        Debug.Log(_levels.Count);

        foreach (var item in _levels)
        {
            var level = JsonUtility.FromJson<Level>(item);
            LevelElement element = Instantiate(_levelElement, _levelElementPanel);
            element.gameObject.SetActive(true);

            element.Initialize(level, this);
        }
    }

    public void Load(Level level)
    {
        _level = FindObjectOfType<Instance>() as ILevel;
        _level.Initialize(GameSettings);
        _level.Build(level);

        _listPanel.gameObject.SetActive(false);
    }


    public void LoadObjects()
    {

    }

    public void Create()
    {
        var id = _levels.Count;

        var dir = Application.dataPath + @"/" + GameSettings.DirectorySettings.LevelsDirectory;

        var level = new Level()
        {
            ID = id
        };

        using (StreamWriter file = File.CreateText(dir + @"/" + id + ".json"))
        {
            file.Write(JsonUtility.ToJson(level));
        }

        Load(level);
    }
}
